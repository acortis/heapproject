﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeapProject
{
    public class Heap<K, V> where K : IComparable<K>
    {

        // List is an ArrayBasedVector
        private List<
                Tuple<K, V>>  // This is a key-value pair, e.g. <2, "Hello">
          array = new List<Tuple<K, V>>();

        public int Count()
        {
            return array.Count;
        }

        public bool IsEmpty()
        {
            return array.IsEmpty();
        }
        
        private int getLeftChildRank(int parentRank)
        {
            return (2 * parentRank) + 1;
        }

        private int getRightChildRank(int parentRank)
        {
            return (2 * parentRank) + 2;
        }

        private int getParentRank(int childRank)
        {
            return (int)Math.Ceiling(
                (childRank / 2.0) - 1);
        }


        public void Add(K key, V value)
        {
            Tuple<K, V> newItem = new Tuple<K, V>(key, value);

            array.Add(  // append
                    newItem
                );      // the new Item is added to the end of the arraybasedvector
                        // this is the "last" node in the heap
                        // therefore, the completeness property is still held
                        // append operation is *amortised* O(1)

            // To maintain the heap order property, call the upheap operation
            upHeap(
                    array.Count - 1 // this is the rank of the last item
                );
        }

        private void upHeap(int rank) // How many times can this method be called recursively? Height of tree, is guaranteed to be O(log n) due to the completeness property
                                      // every time upHeap is called, we go up by a level (we are calling the parentRank!), so, the maximum number of times that upHeap is called is O(log n)
        {
            if (rank == 0) // rank represents the root.. there are no more parents to compare with, the upHeap has finished
            {
                return;
            }
            // else...

            // compare the key at rank, with the key of the parent
            int parentRank = getParentRank(rank);

            // key and parent key are the values that I want to compare
            K key = array[rank].Item1;
            K parentKey = array[parentRank].Item1;

            // key.CompareTo(parentKey) - compares the key of this node with the key of the parent node
            // if the key of the parent is GREATER than the current key,
                    // we have to swap and continue the upHeap
            if (key.CompareTo(parentKey) < 0)
            {
                array.Swap(rank, parentRank); // swap with parent...
                upHeap(parentRank);
            }
            // else.. we are finished.
        }

        public V RemoveMin()
        {
            V output = array[0].Item2; // due to heap order, we are guaranteed that the minimum element is at position 0!

            array[0] = array[array.Count - 1]; // replace the first element with the last element
            array.RemoveAt(array.Count - 1); // the last element can be removed, since we copied it to the first element

            // we have removed the first element (in O(1) time))
            // we have a complete tree
            // however, the element at position 0, may not be the smallest (i.e. heap-order may be violated)

            downHeap(0); // start fixing the heap-order, starting from the root (position 0)

            return output;
        }

        private void downHeap(int rank)
        {
            // rank is the current node in the heap
            // heap has the last node at rank array.count - 1

            int leftChildRank = getLeftChildRank(rank);

            if (leftChildRank > array.Count - 1) // there are no more children to compare with
            {
                // calling array[leftChildRank];
                // will throw a new IndexOutOfRangeException(); - because there are no children

                // this is a stopping condition - we are finished! Heap order is restored!
                return;
            }
            // else there is at least 1 child!

            int smallestChildRank = leftChildRank;

            if (leftChildRank == array.Count - 1) // there is exactly 1 child, and that child is the left child
            {
                // array[leftChildRank] will work! - this is the last element in the array!
                // array[rightChildRank] will throw a new IndexOutOfRangeException();
                // because there is no right child!

                // since there is ONLY 1 child, the smallestChildRank == leftChildRank - done!
            }
            else // leftChildRank < array.Count - 1
            {    // there are 2 children

                // array[leftChildRank] will work!
                // array[rightChildRank] will also work!

                int rightChildRank = leftChildRank + 1;

                // we have two children! So we need to find the smallestChildRank!

                // if the key of the left child is SMALLER than the key of the right child
                // if (array[leftChildRank].Item1 < array[rightChildRank].Item1)
                if (array[leftChildRank].Item1.CompareTo(array[rightChildRank].Item1) < 0) // similar to C++ String.CompareTo
                {
                    // smallestChildRank = leftChildRank; - we already have the right answer!
                }
                else // key of the right child is equal to, or greater than that of the left child
                {
                    smallestChildRank = rightChildRank;
                }
            }

            // I have 1 or more child
            // the smallestChild is at smallestChildRank

            // compare rank with smallestChildRank
            if (array[rank].Item1.CompareTo(array[smallestChildRank].Item1) > 0) // parent's key is bigger than child's key
            {
                array.Swap(rank, smallestChildRank); //swap items!
                downHeap(smallestChildRank);        // recursive call
            }
            // else... there is nothing left to swap, heap-order is restored!
        }
    }
}
