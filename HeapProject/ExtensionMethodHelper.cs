﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeapProject
{
    public static class ExtensionMethodHelper
    {

        /// <summary>
        /// Swap the rank X and rank Y in a List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="rankX"></param>
        /// <param name="rankY"></param>
        public static void Swap<T>(this IList<T> array, int rankX, int rankY)
        {
            T temp = array[rankX];
            array[rankX] = array[rankY];
            array[rankY] = temp;
        }

        public static bool IsEmpty<T>(this List<T> array)
        {
            return array.Count == 0;
        }
    }
}
