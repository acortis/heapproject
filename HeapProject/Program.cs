﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeapProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Heap<int, String> myHeap = new Heap<int, string>();

            // Heap is a "min-Heap", lower value of integer means higher priority!
            myHeap.Add(4, "Task-4");
            myHeap.Add(2, "Task-2");
            myHeap.Add(3, "Task-3");
            myHeap.Add(7, "Task-7");
            myHeap.Add(10, "Task-10");
            myHeap.Add(5, "Task-5");
            myHeap.Add(1, "Task-1");

            List<String> output = new List<string>();

            // while the heap is not empty
            while (!myHeap.IsEmpty())
            {
                // remove the items, one by one
                output.Add(myHeap.RemoveMin());
            }

            // print the items in the order that they have been removed
            foreach (String getTask in output)
            {
                Console.WriteLine(getTask);
            }
            
            Console.ReadKey();
        }
    }
}
